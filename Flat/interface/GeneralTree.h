
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TLorentzVector.h"
#include "TClonesArray.h"
#include "TString.h"
#include "genericTree.h"
#include <map>
class GeneralTree : public genericTree {
	private:
		std::vector<double> betas = {0.5, 1.0, 2.0, 4.0};
		std::vector<int> Ns = {1,2,3,4}; 
		std::vector<int> orders = {1,2,3};
    TString makeECFString(int order, int N, float beta) {
      return TString::Format("ECFN_%i_%i_%.2i",order,N,int(10*beta));
    }

  public:
    GeneralTree();
    ~GeneralTree();
    void ReadTree(TTree *t);
    void WriteTree(TTree *t);
    void Fill() { treePtr->Fill(); }
    void Reset();

    std::vector<double> get_betas() const { return betas; }
    std::vector<int> get_Ns() const { return Ns; }
    std::vector<int> get_orders() const { return orders; }
    
	int runNumber=0;
	int lumiNumber=0;
	ULong64_t eventNumber=0;
	int npv=0;
	float mcWeight=0;
	int trigger=0;
	int metFilter=0;
	float sf_ewkV=0;
	float sf_qcdV=0;
	float sf_lep=0;
	float sf_lepTrack=0;
	float sf_btag0=0;
	float sf_btag0BUp=0;
	float sf_btag0BDown=0;
	float sf_btag0MUp=0;
	float sf_btag0MDown=0;
	float sf_btag1=0;
	float sf_btag1BUp=0;
	float sf_btag1BDown=0;
	float sf_btag1MUp=0;
	float sf_btag1MDown=0;
	float sf_sjbtag0=0;
	float sf_sjbtag0BUp=0;
	float sf_sjbtag0BDown=0;
	float sf_sjbtag0MUp=0;
	float sf_sjbtag0MDown=0;
	float sf_sjbtag1=0;
	float sf_sjbtag1BUp=0;
	float sf_sjbtag1BDown=0;
	float sf_sjbtag1MUp=0;
	float sf_sjbtag1MDown=0;
	float sf_sjbtag2=0;
	float sf_sjbtag2BUp=0;
	float sf_sjbtag2BDown=0;
	float sf_sjbtag2MUp=0;
	float sf_sjbtag2MDown=0;
	float sf_eleTrig=0;
	float sf_phoTrig=0;
	float sf_metTrig=0;
	float sf_pu=0;
	float sf_tt=0;
  float sf_tt_ext=0;
	float sf_phoPurity=0;
	float finalWeight=0;
	float pfmet=0;
	float pfmetphi=0;
	float pfmetnomu=0;
	float puppimet=0;
	float puppimetphi=0;
	float calomet=0;
	float calometphi=0;
	float pfcalobalance=0;
	float sumET=0;
	float trkmet=0;
	float UWmag=0;
	float UWphi=0;
	float UZmag=0;
	float UZphi=0;
	float UAmag=0;
	float UAphi=0;
	float Uperp=0;
	float Upara=0;
	float pfUWmag=0;
	float pfUWphi=0;
	float pfUZmag=0;
	float pfUZphi=0;
	float pfUAmag=0;
	float pfUAphi=0;
	float pfUperp=0;
	float pfUpara=0;
	float dphipfmet=0;
	float dphipuppimet=0;
	float dphiUW=0;
	float dphiUZ=0;
	float dphiUA=0;
	float dphipfUW=0;
	float dphipfUZ=0;
	float dphipfUA=0;
	float trueGenBosonPt=0;
	float genBosonPt=0;
	float genBosonEta=0;
	float genBosonMass=0;
	float genBosonPhi=0;
	float genTopPt=0;
	int nJet=0;
	float jet1Phi=0;
	float jet1Pt=0;
	float jet1Eta=0;
	float jet1CSV=0;
	float jet2Phi=0;
	float jet2Pt=0;
	float jet2Eta=0;
	float jet2CSV=0;
	int jet1IsTight=0;
	float isojet1Pt=0;
	float isojet1CSV=0;
	int isojet1Flav=0;
	float jet12Mass=0;
	float jet12DEta=0;
	int jetNBtags=0;
	int isojetNBtags=0;
	int nFatjet=0;
	float fj1Tau32=0;
	float fj1Tau21=0;
  float fj1Tau32SD=0;
  float fj1Tau21SD=0;
	float fj1MSD=0;
	float fj1Pt=0;
	float fj1Phi=0;
	float fj1Eta=0;
	float fj1MaxCSV=0;
	float fj1MinCSV=0;
	float fj1GenPt=0;
	float fj1GenSize=0;
	int fj1IsMatched=0;
  float fj1GenWPt=0;
  float fj1GenWSize=0;
  int fj1IsWMatched=0;
  int fj1HighestPtGen=0;
  float fj1HighestPtGenPt=-1;
	int fj1IsTight=0;
	int fj1IsLoose=0;
	float fj1RawPt=0;
	int fj1IsHF=0;
	int isHF=0;
	int nLoosePhoton=0;
	int nTightPhoton=0;
	int loosePho1IsTight=0;
	float loosePho1Pt=0;
	float loosePho1Eta=0;
	float loosePho1Phi=0;
	int nLooseLep=0;
	int nLooseElectron=0;
	int nLooseMuon=0;
	int nTightLep=0;
	int nTightElectron=0;
	int nTightMuon=0;
	int looseLep1PdgId=0;
	int looseLep2PdgId=0;
	int looseLep1IsTight=0;
	int looseLep2IsTight=0;
	float looseLep1Pt=0;
	float looseLep1Eta=0;
	float looseLep1Phi=0;
	float looseLep2Pt=0;
	float looseLep2Eta=0;
	float looseLep2Phi=0;
	float diLepMass=0;
	int nTau=0;

	std::map<TString,float> fj1ECFNs;

//ENDDEF
};

GeneralTree::GeneralTree() {
    	runNumber=0;
	lumiNumber=0;
	eventNumber=0;
	npv=0;
	mcWeight=0;
	trigger=0;
	metFilter=0;
  sf_ewkV=1;
  sf_qcdV=1;
  sf_lep=1;
  sf_lepTrack=1;
  sf_btag0=1;
  sf_btag0BUp=1;
  sf_btag0BDown=1;
  sf_btag0MUp=1;
  sf_btag0MDown=1;
  sf_btag1=1;
  sf_btag1BUp=1;
  sf_btag1BDown=1;
  sf_btag1MUp=1;
  sf_btag1MDown=1;
  sf_sjbtag0=1;
  sf_sjbtag0BUp=1;
  sf_sjbtag0BDown=1;
  sf_sjbtag0MUp=1;
  sf_sjbtag0MDown=1;
  sf_sjbtag1=1;
  sf_sjbtag1BUp=1;
  sf_sjbtag1BDown=1;
  sf_sjbtag1MUp=1;
  sf_sjbtag1MDown=1;
  sf_sjbtag2=1;
  sf_sjbtag2BUp=1;
  sf_sjbtag2BDown=1;
  sf_sjbtag2MUp=1;
  sf_sjbtag2MDown=1;
  sf_eleTrig=1;
  sf_phoTrig=1;
  sf_metTrig=1;
  sf_pu=1;
  sf_tt=1;
  sf_tt_ext=1;
  sf_phoPurity=1;
	finalWeight=0;
	pfmet=0;
	pfmetphi=0;
	pfmetnomu=0;
	puppimet=0;
	puppimetphi=0;
	calomet=0;
	calometphi=0;
	pfcalobalance=0;
	sumET=0;
	trkmet=0;
	UWmag=0;
	UWphi=0;
	UZmag=0;
	UZphi=0;
	UAmag=0;
	UAphi=0;
	Uperp=0;
	Upara=0;
	pfUWmag=0;
	pfUWphi=0;
	pfUZmag=0;
	pfUZphi=0;
	pfUAmag=0;
	pfUAphi=0;
	pfUperp=0;
	pfUpara=0;
	dphipfmet=0;
	dphipuppimet=0;
	dphiUW=0;
	dphiUZ=0;
	dphiUA=0;
	dphipfUW=0;
	dphipfUZ=0;
	dphipfUA=0;
	trueGenBosonPt=0;
	genBosonPt=0;
	genBosonEta=0;
	genBosonMass=0;
	genBosonPhi=0;
	genTopPt=0;
	nJet=0;
	jet1Phi=0;
	jet1Pt=0;
	jet1Eta=0;
	jet1CSV=0;
	jet2Phi=0;
	jet2Pt=0;
	jet2Eta=0;
	jet2CSV=0;
	jet1IsTight=0;
	isojet1Pt=0;
	isojet1CSV=0;
	isojet1Flav=0;
	jet12Mass=0;
	jet12DEta=0;
	jetNBtags=0;
	isojetNBtags=0;
	nFatjet=0;
	fj1Tau32=0;
	fj1Tau21=0;
  fj1Tau32SD=0;
  fj1Tau21SD=0;
	fj1MSD=0;
	fj1Pt=0;
	fj1Phi=0;
	fj1Eta=0;
	fj1MaxCSV=0;
	fj1MinCSV=0;
	fj1GenPt=0;
	fj1GenSize=0;
	fj1IsMatched=0;
  fj1GenWPt=0;
  fj1GenWSize=0;
  fj1IsWMatched=0;
  fj1HighestPtGen=0;
  fj1HighestPtGenPt=-1;
	fj1IsTight=0;
	fj1IsLoose=0;
	fj1RawPt=0;
	fj1IsHF=0;
	isHF=0;
	nLoosePhoton=0;
	nTightPhoton=0;
	loosePho1IsTight=0;
	loosePho1Pt=0;
	loosePho1Eta=0;
	loosePho1Phi=0;
	nLooseLep=0;
	nLooseElectron=0;
	nLooseMuon=0;
	nTightLep=0;
	nTightElectron=0;
	nTightMuon=0;
	looseLep1PdgId=0;
	looseLep2PdgId=0;
	looseLep1IsTight=0;
	looseLep2IsTight=0;
	looseLep1Pt=0;
	looseLep1Eta=0;
	looseLep1Phi=0;
	looseLep2Pt=0;
	looseLep2Eta=0;
	looseLep2Phi=0;
	diLepMass=0;
	nTau=0;

  for (auto beta : betas) {
    for (auto N : Ns) {
      for (auto order : orders) {
        TString ecfn(makeECFString(order,N,beta));
        fj1ECFNs[ecfn]=-1;
      }
    }
  }

//ENDCONST
}

GeneralTree::~GeneralTree() {
    //ENDDEST
}

void GeneralTree::Reset() {
    	runNumber = 0;
	lumiNumber = 0;
	eventNumber = 0;
	npv = 0;
	mcWeight = -1;
	trigger = 0;
	metFilter = 0;
  sf_ewkV = 1;
  sf_qcdV = 1;
  sf_lep = 1;
  sf_lepTrack = 1;
  sf_btag0 = 1;
  sf_btag0BUp = 1;
  sf_btag0BDown = 1;
  sf_btag0MUp = 1;
  sf_btag0MDown = 1;
  sf_btag1 = 1;
  sf_btag1BUp = 1;
  sf_btag1BDown = 1;
  sf_btag1MUp = 1;
  sf_btag1MDown = 1;
  sf_sjbtag0 = 1;
  sf_sjbtag0BUp = 1;
  sf_sjbtag0BDown = 1;
  sf_sjbtag0MUp = 1;
  sf_sjbtag0MDown = 1;
  sf_sjbtag1 = 1;
  sf_sjbtag1BUp = 1;
  sf_sjbtag1BDown = 1;
  sf_sjbtag1MUp = 1;
  sf_sjbtag1MDown = 1;
  sf_sjbtag2 = 1;
  sf_sjbtag2BUp = 1;
  sf_sjbtag2BDown = 1;
  sf_sjbtag2MUp = 1;
  sf_sjbtag2MDown = 1;
  sf_eleTrig = 1;
  sf_phoTrig = 1;
  sf_metTrig = 1;
  sf_pu = 1;
  sf_tt = 1;
  sf_tt_ext = 1;
  sf_phoPurity = 1;
	finalWeight = -1;
	pfmet = -1;
	pfmetphi = -1;
	pfmetnomu = -1;
	puppimet = -1;
	puppimetphi = -1;
	calomet = -1;
	calometphi = -1;
	pfcalobalance = -1;
	sumET = -1;
	trkmet = -1;
	UWmag = -1;
	UWphi = -1;
	UZmag = -1;
	UZphi = -1;
	UAmag = -1;
	UAphi = -1;
	Uperp = -1;
	Upara = -1;
	pfUWmag = -1;
	pfUWphi = -1;
	pfUZmag = -1;
	pfUZphi = -1;
	pfUAmag = -1;
	pfUAphi = -1;
	pfUperp = -1;
	pfUpara = -1;
	dphipfmet = -1;
	dphipuppimet = -1;
	dphiUW = -1;
	dphiUZ = -1;
	dphiUA = -1;
	dphipfUW = -1;
	dphipfUZ = -1;
	dphipfUA = -1;
	trueGenBosonPt = -1;
	genBosonPt = -1;
	genBosonEta = -1;
	genBosonMass = -1;
	genBosonPhi = -1;
	genTopPt = -1;
	nJet = 0;
	jet1Phi = -1;
	jet1Pt = -1;
	jet1Eta = -1;
	jet1CSV = -1;
	jet2Phi = -1;
	jet2Pt = -1;
	jet2Eta = -1;
	jet2CSV = -1;
	jet1IsTight = 0;
	isojet1Pt = -1;
	isojet1CSV = -1;
	isojet1Flav = 0;
	jet12Mass = -1;
	jet12DEta = -1;
	jetNBtags = 0;
	isojetNBtags = 0;
	nFatjet = 0;
	fj1Tau32 = -1;
	fj1Tau21 = -1;
  fj1Tau32SD = -1;
  fj1Tau21SD = -1;
	fj1MSD = -1;
	fj1Pt = -1;
	fj1Phi = -1;
	fj1Eta = -1;
	fj1MaxCSV = -1;
	fj1MinCSV = -1;
	fj1GenPt = -1;
	fj1GenSize = -1;
	fj1IsMatched = 0;
  fj1GenWPt = -1;
  fj1GenWSize = -1;
  fj1IsWMatched = 0;
  fj1HighestPtGen=0;
  fj1HighestPtGenPt=-1;
	fj1IsTight = 0;
	fj1IsLoose = 0;
	fj1RawPt = -1;
	fj1IsHF = 0;
	isHF = 0;
	nLoosePhoton = 0;
	nTightPhoton = 0;
	loosePho1IsTight = 0;
	loosePho1Pt = -1;
	loosePho1Eta = -1;
	loosePho1Phi = -1;
	nLooseLep = 0;
	nLooseElectron = 0;
	nLooseMuon = 0;
	nTightLep = 0;
	nTightElectron = 0;
	nTightMuon = 0;
	looseLep1PdgId = 0;
	looseLep2PdgId = 0;
	looseLep1IsTight = 0;
	looseLep2IsTight = 0;
	looseLep1Pt = -1;
	looseLep1Eta = -1;
	looseLep1Phi = -1;
	looseLep2Pt = -1;
	looseLep2Eta = -1;
	looseLep2Phi = -1;
	diLepMass = -1;
	nTau = 0;

  for (auto beta : betas) {
    for (auto N : Ns) {
      for (auto order : orders) {
        TString ecfn(makeECFString(order,N,beta));
        fj1ECFNs[ecfn]=-1;
      }
    }
  }

//ENDRESET
}

void GeneralTree::ReadTree(TTree *t) {
      treePtr = t;
      treePtr->SetBranchStatus("*",0);
    	treePtr->SetBranchStatus("runNumber",1);
	treePtr->SetBranchAddress("runNumber",&runNumber);
	treePtr->SetBranchStatus("lumiNumber",1);
	treePtr->SetBranchAddress("lumiNumber",&lumiNumber);
	treePtr->SetBranchStatus("eventNumber",1);
	treePtr->SetBranchAddress("eventNumber",&eventNumber);
	treePtr->SetBranchStatus("npv",1);
	treePtr->SetBranchAddress("npv",&npv);
	treePtr->SetBranchStatus("mcWeight",1);
	treePtr->SetBranchAddress("mcWeight",&mcWeight);
	treePtr->SetBranchStatus("trigger",1);
	treePtr->SetBranchAddress("trigger",&trigger);
	treePtr->SetBranchStatus("metFilter",1);
	treePtr->SetBranchAddress("metFilter",&metFilter);
	treePtr->SetBranchStatus("sf_ewkV",1);
	treePtr->SetBranchAddress("sf_ewkV",&sf_ewkV);
	treePtr->SetBranchStatus("sf_qcdV",1);
	treePtr->SetBranchAddress("sf_qcdV",&sf_qcdV);
	treePtr->SetBranchStatus("sf_lep",1);
	treePtr->SetBranchAddress("sf_lep",&sf_lep);
	treePtr->SetBranchStatus("sf_lepTrack",1);
	treePtr->SetBranchAddress("sf_lepTrack",&sf_lepTrack);
	treePtr->SetBranchStatus("sf_btag0",1);
	treePtr->SetBranchAddress("sf_btag0",&sf_btag0);
	treePtr->SetBranchStatus("sf_btag0BUp",1);
	treePtr->SetBranchAddress("sf_btag0BUp",&sf_btag0BUp);
	treePtr->SetBranchStatus("sf_btag0BDown",1);
	treePtr->SetBranchAddress("sf_btag0BDown",&sf_btag0BDown);
	treePtr->SetBranchStatus("sf_btag0MUp",1);
	treePtr->SetBranchAddress("sf_btag0MUp",&sf_btag0MUp);
	treePtr->SetBranchStatus("sf_btag0MDown",1);
	treePtr->SetBranchAddress("sf_btag0MDown",&sf_btag0MDown);
	treePtr->SetBranchStatus("sf_btag1",1);
	treePtr->SetBranchAddress("sf_btag1",&sf_btag1);
	treePtr->SetBranchStatus("sf_btag1BUp",1);
	treePtr->SetBranchAddress("sf_btag1BUp",&sf_btag1BUp);
	treePtr->SetBranchStatus("sf_btag1BDown",1);
	treePtr->SetBranchAddress("sf_btag1BDown",&sf_btag1BDown);
	treePtr->SetBranchStatus("sf_btag1MUp",1);
	treePtr->SetBranchAddress("sf_btag1MUp",&sf_btag1MUp);
	treePtr->SetBranchStatus("sf_btag1MDown",1);
	treePtr->SetBranchAddress("sf_btag1MDown",&sf_btag1MDown);
	treePtr->SetBranchStatus("sf_sjbtag0",1);
	treePtr->SetBranchAddress("sf_sjbtag0",&sf_sjbtag0);
	treePtr->SetBranchStatus("sf_sjbtag0BUp",1);
	treePtr->SetBranchAddress("sf_sjbtag0BUp",&sf_sjbtag0BUp);
	treePtr->SetBranchStatus("sf_sjbtag0BDown",1);
	treePtr->SetBranchAddress("sf_sjbtag0BDown",&sf_sjbtag0BDown);
	treePtr->SetBranchStatus("sf_sjbtag0MUp",1);
	treePtr->SetBranchAddress("sf_sjbtag0MUp",&sf_sjbtag0MUp);
	treePtr->SetBranchStatus("sf_sjbtag0MDown",1);
	treePtr->SetBranchAddress("sf_sjbtag0MDown",&sf_sjbtag0MDown);
	treePtr->SetBranchStatus("sf_sjbtag1",1);
	treePtr->SetBranchAddress("sf_sjbtag1",&sf_sjbtag1);
	treePtr->SetBranchStatus("sf_sjbtag1BUp",1);
	treePtr->SetBranchAddress("sf_sjbtag1BUp",&sf_sjbtag1BUp);
	treePtr->SetBranchStatus("sf_sjbtag1BDown",1);
	treePtr->SetBranchAddress("sf_sjbtag1BDown",&sf_sjbtag1BDown);
	treePtr->SetBranchStatus("sf_sjbtag1MUp",1);
	treePtr->SetBranchAddress("sf_sjbtag1MUp",&sf_sjbtag1MUp);
	treePtr->SetBranchStatus("sf_sjbtag1MDown",1);
	treePtr->SetBranchAddress("sf_sjbtag1MDown",&sf_sjbtag1MDown);
	treePtr->SetBranchStatus("sf_sjbtag2",1);
	treePtr->SetBranchAddress("sf_sjbtag2",&sf_sjbtag2);
	treePtr->SetBranchStatus("sf_sjbtag2BUp",1);
	treePtr->SetBranchAddress("sf_sjbtag2BUp",&sf_sjbtag2BUp);
	treePtr->SetBranchStatus("sf_sjbtag2BDown",1);
	treePtr->SetBranchAddress("sf_sjbtag2BDown",&sf_sjbtag2BDown);
	treePtr->SetBranchStatus("sf_sjbtag2MUp",1);
	treePtr->SetBranchAddress("sf_sjbtag2MUp",&sf_sjbtag2MUp);
	treePtr->SetBranchStatus("sf_sjbtag2MDown",1);
	treePtr->SetBranchAddress("sf_sjbtag2MDown",&sf_sjbtag2MDown);
	treePtr->SetBranchStatus("sf_eleTrig",1);
	treePtr->SetBranchAddress("sf_eleTrig",&sf_eleTrig);
	treePtr->SetBranchStatus("sf_phoTrig",1);
	treePtr->SetBranchAddress("sf_phoTrig",&sf_phoTrig);
	treePtr->SetBranchStatus("sf_metTrig",1);
	treePtr->SetBranchAddress("sf_metTrig",&sf_metTrig);
	treePtr->SetBranchStatus("sf_pu",1);
	treePtr->SetBranchAddress("sf_pu",&sf_pu);
	treePtr->SetBranchStatus("sf_tt",1);
	treePtr->SetBranchAddress("sf_tt",&sf_tt);
  treePtr->SetBranchStatus("sf_tt_ext",1);
  treePtr->SetBranchAddress("sf_tt_ext",&sf_tt_ext);
	treePtr->SetBranchStatus("sf_phoPurity",1);
	treePtr->SetBranchAddress("sf_phoPurity",&sf_phoPurity);
	treePtr->SetBranchStatus("finalWeight",1);
	treePtr->SetBranchAddress("finalWeight",&finalWeight);
	treePtr->SetBranchStatus("pfmet",1);
	treePtr->SetBranchAddress("pfmet",&pfmet);
	treePtr->SetBranchStatus("pfmetphi",1);
	treePtr->SetBranchAddress("pfmetphi",&pfmetphi);
	treePtr->SetBranchStatus("pfmetnomu",1);
	treePtr->SetBranchAddress("pfmetnomu",&pfmetnomu);
	treePtr->SetBranchStatus("puppimet",1);
	treePtr->SetBranchAddress("puppimet",&puppimet);
	treePtr->SetBranchStatus("puppimetphi",1);
	treePtr->SetBranchAddress("puppimetphi",&puppimetphi);
	treePtr->SetBranchStatus("calomet",1);
	treePtr->SetBranchAddress("calomet",&calomet);
	treePtr->SetBranchStatus("calometphi",1);
	treePtr->SetBranchAddress("calometphi",&calometphi);
	treePtr->SetBranchStatus("pfcalobalance",1);
	treePtr->SetBranchAddress("pfcalobalance",&pfcalobalance);
	treePtr->SetBranchStatus("sumET",1);
	treePtr->SetBranchAddress("sumET",&sumET);
	treePtr->SetBranchStatus("trkmet",1);
	treePtr->SetBranchAddress("trkmet",&trkmet);
	treePtr->SetBranchStatus("UWmag",1);
	treePtr->SetBranchAddress("UWmag",&UWmag);
	treePtr->SetBranchStatus("UWphi",1);
	treePtr->SetBranchAddress("UWphi",&UWphi);
	treePtr->SetBranchStatus("UZmag",1);
	treePtr->SetBranchAddress("UZmag",&UZmag);
	treePtr->SetBranchStatus("UZphi",1);
	treePtr->SetBranchAddress("UZphi",&UZphi);
	treePtr->SetBranchStatus("UAmag",1);
	treePtr->SetBranchAddress("UAmag",&UAmag);
	treePtr->SetBranchStatus("UAphi",1);
	treePtr->SetBranchAddress("UAphi",&UAphi);
	treePtr->SetBranchStatus("Uperp",1);
	treePtr->SetBranchAddress("Uperp",&Uperp);
	treePtr->SetBranchStatus("Upara",1);
	treePtr->SetBranchAddress("Upara",&Upara);
	treePtr->SetBranchStatus("pfUWmag",1);
	treePtr->SetBranchAddress("pfUWmag",&pfUWmag);
	treePtr->SetBranchStatus("pfUWphi",1);
	treePtr->SetBranchAddress("pfUWphi",&pfUWphi);
	treePtr->SetBranchStatus("pfUZmag",1);
	treePtr->SetBranchAddress("pfUZmag",&pfUZmag);
	treePtr->SetBranchStatus("pfUZphi",1);
	treePtr->SetBranchAddress("pfUZphi",&pfUZphi);
	treePtr->SetBranchStatus("pfUAmag",1);
	treePtr->SetBranchAddress("pfUAmag",&pfUAmag);
	treePtr->SetBranchStatus("pfUAphi",1);
	treePtr->SetBranchAddress("pfUAphi",&pfUAphi);
	treePtr->SetBranchStatus("pfUperp",1);
	treePtr->SetBranchAddress("pfUperp",&pfUperp);
	treePtr->SetBranchStatus("pfUpara",1);
	treePtr->SetBranchAddress("pfUpara",&pfUpara);
	treePtr->SetBranchStatus("dphipfmet",1);
	treePtr->SetBranchAddress("dphipfmet",&dphipfmet);
	treePtr->SetBranchStatus("dphipuppimet",1);
	treePtr->SetBranchAddress("dphipuppimet",&dphipuppimet);
	treePtr->SetBranchStatus("dphiUW",1);
	treePtr->SetBranchAddress("dphiUW",&dphiUW);
	treePtr->SetBranchStatus("dphiUZ",1);
	treePtr->SetBranchAddress("dphiUZ",&dphiUZ);
	treePtr->SetBranchStatus("dphiUA",1);
	treePtr->SetBranchAddress("dphiUA",&dphiUA);
	treePtr->SetBranchStatus("dphipfUW",1);
	treePtr->SetBranchAddress("dphipfUW",&dphipfUW);
	treePtr->SetBranchStatus("dphipfUZ",1);
	treePtr->SetBranchAddress("dphipfUZ",&dphipfUZ);
	treePtr->SetBranchStatus("dphipfUA",1);
	treePtr->SetBranchAddress("dphipfUA",&dphipfUA);
	treePtr->SetBranchStatus("trueGenBosonPt",1);
	treePtr->SetBranchAddress("trueGenBosonPt",&trueGenBosonPt);
	treePtr->SetBranchStatus("genBosonPt",1);
	treePtr->SetBranchAddress("genBosonPt",&genBosonPt);
	treePtr->SetBranchStatus("genBosonEta",1);
	treePtr->SetBranchAddress("genBosonEta",&genBosonEta);
	treePtr->SetBranchStatus("genBosonMass",1);
	treePtr->SetBranchAddress("genBosonMass",&genBosonMass);
	treePtr->SetBranchStatus("genBosonPhi",1);
	treePtr->SetBranchAddress("genBosonPhi",&genBosonPhi);
	treePtr->SetBranchStatus("genTopPt",1);
	treePtr->SetBranchAddress("genTopPt",&genTopPt);
	treePtr->SetBranchStatus("nJet",1);
	treePtr->SetBranchAddress("nJet",&nJet);
	treePtr->SetBranchStatus("jet1Phi",1);
	treePtr->SetBranchAddress("jet1Phi",&jet1Phi);
	treePtr->SetBranchStatus("jet1Pt",1);
	treePtr->SetBranchAddress("jet1Pt",&jet1Pt);
	treePtr->SetBranchStatus("jet1Eta",1);
	treePtr->SetBranchAddress("jet1Eta",&jet1Eta);
	treePtr->SetBranchStatus("jet1CSV",1);
	treePtr->SetBranchAddress("jet1CSV",&jet1CSV);
	treePtr->SetBranchStatus("jet2Phi",1);
	treePtr->SetBranchAddress("jet2Phi",&jet2Phi);
	treePtr->SetBranchStatus("jet2Pt",1);
	treePtr->SetBranchAddress("jet2Pt",&jet2Pt);
	treePtr->SetBranchStatus("jet2Eta",1);
	treePtr->SetBranchAddress("jet2Eta",&jet2Eta);
	treePtr->SetBranchStatus("jet2CSV",1);
	treePtr->SetBranchAddress("jet2CSV",&jet2CSV);
	treePtr->SetBranchStatus("jet1IsTight",1);
	treePtr->SetBranchAddress("jet1IsTight",&jet1IsTight);
	treePtr->SetBranchStatus("isojet1Pt",1);
	treePtr->SetBranchAddress("isojet1Pt",&isojet1Pt);
	treePtr->SetBranchStatus("isojet1CSV",1);
	treePtr->SetBranchAddress("isojet1CSV",&isojet1CSV);
	treePtr->SetBranchStatus("isojet1Flav",1);
	treePtr->SetBranchAddress("isojet1Flav",&isojet1Flav);
	treePtr->SetBranchStatus("jet12Mass",1);
	treePtr->SetBranchAddress("jet12Mass",&jet12Mass);
	treePtr->SetBranchStatus("jet12DEta",1);
	treePtr->SetBranchAddress("jet12DEta",&jet12DEta);
	treePtr->SetBranchStatus("jetNBtags",1);
	treePtr->SetBranchAddress("jetNBtags",&jetNBtags);
	treePtr->SetBranchStatus("isojetNBtags",1);
	treePtr->SetBranchAddress("isojetNBtags",&isojetNBtags);
	treePtr->SetBranchStatus("nFatjet",1);
	treePtr->SetBranchAddress("nFatjet",&nFatjet);
	treePtr->SetBranchStatus("fj1Tau32",1);
	treePtr->SetBranchAddress("fj1Tau32",&fj1Tau32);
	treePtr->SetBranchStatus("fj1Tau21",1);
	treePtr->SetBranchAddress("fj1Tau21",&fj1Tau21);
  treePtr->SetBranchStatus("fj1Tau32SD",1);
  treePtr->SetBranchAddress("fj1Tau32SD",&fj1Tau32SD);
  treePtr->SetBranchStatus("fj1Tau21SD",1);
  treePtr->SetBranchAddress("fj1Tau21SD",&fj1Tau21SD);
	treePtr->SetBranchStatus("fj1MSD",1);
	treePtr->SetBranchAddress("fj1MSD",&fj1MSD);
	treePtr->SetBranchStatus("fj1Pt",1);
	treePtr->SetBranchAddress("fj1Pt",&fj1Pt);
	treePtr->SetBranchStatus("fj1Phi",1);
	treePtr->SetBranchAddress("fj1Phi",&fj1Phi);
	treePtr->SetBranchStatus("fj1Eta",1);
	treePtr->SetBranchAddress("fj1Eta",&fj1Eta);
	treePtr->SetBranchStatus("fj1MaxCSV",1);
	treePtr->SetBranchAddress("fj1MaxCSV",&fj1MaxCSV);
	treePtr->SetBranchStatus("fj1MinCSV",1);
	treePtr->SetBranchAddress("fj1MinCSV",&fj1MinCSV);
	treePtr->SetBranchStatus("fj1GenPt",1);
	treePtr->SetBranchAddress("fj1GenPt",&fj1GenPt);
	treePtr->SetBranchStatus("fj1GenSize",1);
	treePtr->SetBranchAddress("fj1GenSize",&fj1GenSize);
	treePtr->SetBranchStatus("fj1IsMatched",1);
	treePtr->SetBranchAddress("fj1IsMatched",&fj1IsMatched);
  treePtr->SetBranchAddress("fj1HighestPtGen",&fj1HighestPtGen);
  treePtr->SetBranchAddress("fj1HighestPtGenPt",&fj1HighestPtGenPt);
	treePtr->SetBranchStatus("fj1IsTight",1);
	treePtr->SetBranchAddress("fj1IsTight",&fj1IsTight);
	treePtr->SetBranchStatus("fj1IsLoose",1);
	treePtr->SetBranchAddress("fj1IsLoose",&fj1IsLoose);
	treePtr->SetBranchStatus("fj1RawPt",1);
	treePtr->SetBranchAddress("fj1RawPt",&fj1RawPt);
	treePtr->SetBranchStatus("fj1IsHF",1);
	treePtr->SetBranchAddress("fj1IsHF",&fj1IsHF);
	treePtr->SetBranchStatus("isHF",1);
	treePtr->SetBranchAddress("isHF",&isHF);
	treePtr->SetBranchStatus("nLoosePhoton",1);
	treePtr->SetBranchAddress("nLoosePhoton",&nLoosePhoton);
	treePtr->SetBranchStatus("nTightPhoton",1);
	treePtr->SetBranchAddress("nTightPhoton",&nTightPhoton);
	treePtr->SetBranchStatus("loosePho1IsTight",1);
	treePtr->SetBranchAddress("loosePho1IsTight",&loosePho1IsTight);
	treePtr->SetBranchStatus("loosePho1Pt",1);
	treePtr->SetBranchAddress("loosePho1Pt",&loosePho1Pt);
	treePtr->SetBranchStatus("loosePho1Eta",1);
	treePtr->SetBranchAddress("loosePho1Eta",&loosePho1Eta);
	treePtr->SetBranchStatus("loosePho1Phi",1);
	treePtr->SetBranchAddress("loosePho1Phi",&loosePho1Phi);
	treePtr->SetBranchStatus("nLooseLep",1);
	treePtr->SetBranchAddress("nLooseLep",&nLooseLep);
	treePtr->SetBranchStatus("nLooseElectron",1);
	treePtr->SetBranchAddress("nLooseElectron",&nLooseElectron);
	treePtr->SetBranchStatus("nLooseMuon",1);
	treePtr->SetBranchAddress("nLooseMuon",&nLooseMuon);
	treePtr->SetBranchStatus("nTightLep",1);
	treePtr->SetBranchAddress("nTightLep",&nTightLep);
	treePtr->SetBranchStatus("nTightElectron",1);
	treePtr->SetBranchAddress("nTightElectron",&nTightElectron);
	treePtr->SetBranchStatus("nTightMuon",1);
	treePtr->SetBranchAddress("nTightMuon",&nTightMuon);
	treePtr->SetBranchStatus("looseLep1PdgId",1);
	treePtr->SetBranchAddress("looseLep1PdgId",&looseLep1PdgId);
	treePtr->SetBranchStatus("looseLep2PdgId",1);
	treePtr->SetBranchAddress("looseLep2PdgId",&looseLep2PdgId);
	treePtr->SetBranchStatus("looseLep1IsTight",1);
	treePtr->SetBranchAddress("looseLep1IsTight",&looseLep1IsTight);
	treePtr->SetBranchStatus("looseLep2IsTight",1);
	treePtr->SetBranchAddress("looseLep2IsTight",&looseLep2IsTight);
	treePtr->SetBranchStatus("looseLep1Pt",1);
	treePtr->SetBranchAddress("looseLep1Pt",&looseLep1Pt);
	treePtr->SetBranchStatus("looseLep1Eta",1);
	treePtr->SetBranchAddress("looseLep1Eta",&looseLep1Eta);
	treePtr->SetBranchStatus("looseLep1Phi",1);
	treePtr->SetBranchAddress("looseLep1Phi",&looseLep1Phi);
	treePtr->SetBranchStatus("looseLep2Pt",1);
	treePtr->SetBranchAddress("looseLep2Pt",&looseLep2Pt);
	treePtr->SetBranchStatus("looseLep2Eta",1);
	treePtr->SetBranchAddress("looseLep2Eta",&looseLep2Eta);
	treePtr->SetBranchStatus("looseLep2Phi",1);
	treePtr->SetBranchAddress("looseLep2Phi",&looseLep2Phi);
	treePtr->SetBranchStatus("diLepMass",1);
	treePtr->SetBranchAddress("diLepMass",&diLepMass);
	treePtr->SetBranchStatus("nTau",1);
	treePtr->SetBranchAddress("nTau",&nTau);
//ENDREAD
}

void GeneralTree::WriteTree(TTree *t) {
      treePtr = t;
    	treePtr->Branch("runNumber",&runNumber,"runNumber/I");
	treePtr->Branch("lumiNumber",&lumiNumber,"lumiNumber/I");
	treePtr->Branch("eventNumber",&eventNumber,"eventNumber/l");
	treePtr->Branch("npv",&npv,"npv/I");
	treePtr->Branch("mcWeight",&mcWeight,"mcWeight/F");
	treePtr->Branch("trigger",&trigger,"trigger/I");
	treePtr->Branch("metFilter",&metFilter,"metFilter/I");
	treePtr->Branch("sf_ewkV",&sf_ewkV,"sf_ewkV/F");
	treePtr->Branch("sf_qcdV",&sf_qcdV,"sf_qcdV/F");
	treePtr->Branch("sf_lep",&sf_lep,"sf_lep/F");
	treePtr->Branch("sf_lepTrack",&sf_lepTrack,"sf_lepTrack/F");
	treePtr->Branch("sf_btag0",&sf_btag0,"sf_btag0/F");
	treePtr->Branch("sf_btag0BUp",&sf_btag0BUp,"sf_btag0BUp/F");
	treePtr->Branch("sf_btag0BDown",&sf_btag0BDown,"sf_btag0BDown/F");
	treePtr->Branch("sf_btag0MUp",&sf_btag0MUp,"sf_btag0MUp/F");
	treePtr->Branch("sf_btag0MDown",&sf_btag0MDown,"sf_btag0MDown/F");
	treePtr->Branch("sf_btag1",&sf_btag1,"sf_btag1/F");
	treePtr->Branch("sf_btag1BUp",&sf_btag1BUp,"sf_btag1BUp/F");
	treePtr->Branch("sf_btag1BDown",&sf_btag1BDown,"sf_btag1BDown/F");
	treePtr->Branch("sf_btag1MUp",&sf_btag1MUp,"sf_btag1MUp/F");
	treePtr->Branch("sf_btag1MDown",&sf_btag1MDown,"sf_btag1MDown/F");
	treePtr->Branch("sf_sjbtag0",&sf_sjbtag0,"sf_sjbtag0/F");
	treePtr->Branch("sf_sjbtag0BUp",&sf_sjbtag0BUp,"sf_sjbtag0BUp/F");
	treePtr->Branch("sf_sjbtag0BDown",&sf_sjbtag0BDown,"sf_sjbtag0BDown/F");
	treePtr->Branch("sf_sjbtag0MUp",&sf_sjbtag0MUp,"sf_sjbtag0MUp/F");
	treePtr->Branch("sf_sjbtag0MDown",&sf_sjbtag0MDown,"sf_sjbtag0MDown/F");
	treePtr->Branch("sf_sjbtag1",&sf_sjbtag1,"sf_sjbtag1/F");
	treePtr->Branch("sf_sjbtag1BUp",&sf_sjbtag1BUp,"sf_sjbtag1BUp/F");
	treePtr->Branch("sf_sjbtag1BDown",&sf_sjbtag1BDown,"sf_sjbtag1BDown/F");
	treePtr->Branch("sf_sjbtag1MUp",&sf_sjbtag1MUp,"sf_sjbtag1MUp/F");
	treePtr->Branch("sf_sjbtag1MDown",&sf_sjbtag1MDown,"sf_sjbtag1MDown/F");
	treePtr->Branch("sf_sjbtag2",&sf_sjbtag2,"sf_sjbtag2/F");
	treePtr->Branch("sf_sjbtag2BUp",&sf_sjbtag2BUp,"sf_sjbtag2BUp/F");
	treePtr->Branch("sf_sjbtag2BDown",&sf_sjbtag2BDown,"sf_sjbtag2BDown/F");
	treePtr->Branch("sf_sjbtag2MUp",&sf_sjbtag2MUp,"sf_sjbtag2MUp/F");
	treePtr->Branch("sf_sjbtag2MDown",&sf_sjbtag2MDown,"sf_sjbtag2MDown/F");
	treePtr->Branch("sf_eleTrig",&sf_eleTrig,"sf_eleTrig/F");
	treePtr->Branch("sf_phoTrig",&sf_phoTrig,"sf_phoTrig/F");
	treePtr->Branch("sf_metTrig",&sf_metTrig,"sf_metTrig/F");
	treePtr->Branch("sf_pu",&sf_pu,"sf_pu/F");
	treePtr->Branch("sf_tt",&sf_tt,"sf_tt/F");
  treePtr->Branch("sf_tt_ext",&sf_tt_ext,"sf_tt_ext/F");
	treePtr->Branch("sf_phoPurity",&sf_phoPurity,"sf_phoPurity/F");
	treePtr->Branch("finalWeight",&finalWeight,"finalWeight/F");
	treePtr->Branch("pfmet",&pfmet,"pfmet/F");
	treePtr->Branch("pfmetphi",&pfmetphi,"pfmetphi/F");
	treePtr->Branch("pfmetnomu",&pfmetnomu,"pfmetnomu/F");
	treePtr->Branch("puppimet",&puppimet,"puppimet/F");
	treePtr->Branch("puppimetphi",&puppimetphi,"puppimetphi/F");
	treePtr->Branch("calomet",&calomet,"calomet/F");
	treePtr->Branch("calometphi",&calometphi,"calometphi/F");
	treePtr->Branch("pfcalobalance",&pfcalobalance,"pfcalobalance/F");
	treePtr->Branch("sumET",&sumET,"sumET/F");
	treePtr->Branch("trkmet",&trkmet,"trkmet/F");
	treePtr->Branch("UWmag",&UWmag,"UWmag/F");
	treePtr->Branch("UWphi",&UWphi,"UWphi/F");
	treePtr->Branch("UZmag",&UZmag,"UZmag/F");
	treePtr->Branch("UZphi",&UZphi,"UZphi/F");
	treePtr->Branch("UAmag",&UAmag,"UAmag/F");
	treePtr->Branch("UAphi",&UAphi,"UAphi/F");
	treePtr->Branch("Uperp",&Uperp,"Uperp/F");
	treePtr->Branch("Upara",&Upara,"Upara/F");
	treePtr->Branch("pfUWmag",&pfUWmag,"pfUWmag/F");
	treePtr->Branch("pfUWphi",&pfUWphi,"pfUWphi/F");
	treePtr->Branch("pfUZmag",&pfUZmag,"pfUZmag/F");
	treePtr->Branch("pfUZphi",&pfUZphi,"pfUZphi/F");
	treePtr->Branch("pfUAmag",&pfUAmag,"pfUAmag/F");
	treePtr->Branch("pfUAphi",&pfUAphi,"pfUAphi/F");
	treePtr->Branch("pfUperp",&pfUperp,"pfUperp/F");
	treePtr->Branch("pfUpara",&pfUpara,"pfUpara/F");
	treePtr->Branch("dphipfmet",&dphipfmet,"dphipfmet/F");
	treePtr->Branch("dphipuppimet",&dphipuppimet,"dphipuppimet/F");
	treePtr->Branch("dphiUW",&dphiUW,"dphiUW/F");
	treePtr->Branch("dphiUZ",&dphiUZ,"dphiUZ/F");
	treePtr->Branch("dphiUA",&dphiUA,"dphiUA/F");
	treePtr->Branch("dphipfUW",&dphipfUW,"dphipfUW/F");
	treePtr->Branch("dphipfUZ",&dphipfUZ,"dphipfUZ/F");
	treePtr->Branch("dphipfUA",&dphipfUA,"dphipfUA/F");
	treePtr->Branch("trueGenBosonPt",&trueGenBosonPt,"trueGenBosonPt/F");
	treePtr->Branch("genBosonPt",&genBosonPt,"genBosonPt/F");
	treePtr->Branch("genBosonEta",&genBosonEta,"genBosonEta/F");
	treePtr->Branch("genBosonMass",&genBosonMass,"genBosonMass/F");
	treePtr->Branch("genBosonPhi",&genBosonPhi,"genBosonPhi/F");
	treePtr->Branch("genTopPt",&genTopPt,"genTopPt/F");
	treePtr->Branch("nJet",&nJet,"nJet/I");
	treePtr->Branch("jet1Phi",&jet1Phi,"jet1Phi/F");
	treePtr->Branch("jet1Pt",&jet1Pt,"jet1Pt/F");
	treePtr->Branch("jet1Eta",&jet1Eta,"jet1Eta/F");
	treePtr->Branch("jet1CSV",&jet1CSV,"jet1CSV/F");
	treePtr->Branch("jet2Phi",&jet2Phi,"jet2Phi/F");
	treePtr->Branch("jet2Pt",&jet2Pt,"jet2Pt/F");
	treePtr->Branch("jet2Eta",&jet2Eta,"jet2Eta/F");
	treePtr->Branch("jet2CSV",&jet2CSV,"jet2CSV/F");
	treePtr->Branch("jet1IsTight",&jet1IsTight,"jet1IsTight/I");
	treePtr->Branch("isojet1Pt",&isojet1Pt,"isojet1Pt/F");
	treePtr->Branch("isojet1CSV",&isojet1CSV,"isojet1CSV/F");
	treePtr->Branch("isojet1Flav",&isojet1Flav,"isojet1Flav/I");
	treePtr->Branch("jet12Mass",&jet12Mass,"jet12Mass/F");
	treePtr->Branch("jet12DEta",&jet12DEta,"jet12DEta/F");
	treePtr->Branch("jetNBtags",&jetNBtags,"jetNBtags/I");
	treePtr->Branch("isojetNBtags",&isojetNBtags,"isojetNBtags/I");
	treePtr->Branch("nFatjet",&nFatjet,"nFatjet/I");
	treePtr->Branch("fj1Tau32",&fj1Tau32,"fj1Tau32/F");
	treePtr->Branch("fj1Tau21",&fj1Tau21,"fj1Tau21/F");
  treePtr->Branch("fj1Tau32SD",&fj1Tau32SD,"fj1Tau32SD/F");
  treePtr->Branch("fj1Tau21SD",&fj1Tau21SD,"fj1Tau21SD/F");
	treePtr->Branch("fj1MSD",&fj1MSD,"fj1MSD/F");
	treePtr->Branch("fj1Pt",&fj1Pt,"fj1Pt/F");
	treePtr->Branch("fj1Phi",&fj1Phi,"fj1Phi/F");
	treePtr->Branch("fj1Eta",&fj1Eta,"fj1Eta/F");
	treePtr->Branch("fj1MaxCSV",&fj1MaxCSV,"fj1MaxCSV/F");
	treePtr->Branch("fj1MinCSV",&fj1MinCSV,"fj1MinCSV/F");
	treePtr->Branch("fj1GenPt",&fj1GenPt,"fj1GenPt/F");
	treePtr->Branch("fj1GenSize",&fj1GenSize,"fj1GenSize/F");
	treePtr->Branch("fj1IsMatched",&fj1IsMatched,"fj1IsMatched/I");
  treePtr->Branch("fj1GenWPt",&fj1GenWPt,"fj1GenWPt/F");
  treePtr->Branch("fj1GenWSize",&fj1GenWSize,"fj1GenWSize/F");
  treePtr->Branch("fj1IsWMatched",&fj1IsWMatched,"fj1IsWMatched/I");
  treePtr->Branch("fj1HighestPtGen",&fj1HighestPtGen,"fj1HighestPtGen/I");
  treePtr->Branch("fj1HighestPtGenPt",&fj1HighestPtGenPt,"fj1HighestPtGenPt/F");
	treePtr->Branch("fj1IsTight",&fj1IsTight,"fj1IsTight/I");
	treePtr->Branch("fj1IsLoose",&fj1IsLoose,"fj1IsLoose/I");
	treePtr->Branch("fj1RawPt",&fj1RawPt,"fj1RawPt/F");
	treePtr->Branch("fj1IsHF",&fj1IsHF,"fj1IsHF/I");
	treePtr->Branch("isHF",&isHF,"isHF/I");
	treePtr->Branch("nLoosePhoton",&nLoosePhoton,"nLoosePhoton/I");
	treePtr->Branch("nTightPhoton",&nTightPhoton,"nTightPhoton/I");
	treePtr->Branch("loosePho1IsTight",&loosePho1IsTight,"loosePho1IsTight/I");
	treePtr->Branch("loosePho1Pt",&loosePho1Pt,"loosePho1Pt/F");
	treePtr->Branch("loosePho1Eta",&loosePho1Eta,"loosePho1Eta/F");
	treePtr->Branch("loosePho1Phi",&loosePho1Phi,"loosePho1Phi/F");
	treePtr->Branch("nLooseLep",&nLooseLep,"nLooseLep/I");
	treePtr->Branch("nLooseElectron",&nLooseElectron,"nLooseElectron/I");
	treePtr->Branch("nLooseMuon",&nLooseMuon,"nLooseMuon/I");
	treePtr->Branch("nTightLep",&nTightLep,"nTightLep/I");
	treePtr->Branch("nTightElectron",&nTightElectron,"nTightElectron/I");
	treePtr->Branch("nTightMuon",&nTightMuon,"nTightMuon/I");
	treePtr->Branch("looseLep1PdgId",&looseLep1PdgId,"looseLep1PdgId/I");
	treePtr->Branch("looseLep2PdgId",&looseLep2PdgId,"looseLep2PdgId/I");
	treePtr->Branch("looseLep1IsTight",&looseLep1IsTight,"looseLep1IsTight/I");
	treePtr->Branch("looseLep2IsTight",&looseLep2IsTight,"looseLep2IsTight/I");
	treePtr->Branch("looseLep1Pt",&looseLep1Pt,"looseLep1Pt/F");
	treePtr->Branch("looseLep1Eta",&looseLep1Eta,"looseLep1Eta/F");
	treePtr->Branch("looseLep1Phi",&looseLep1Phi,"looseLep1Phi/F");
	treePtr->Branch("looseLep2Pt",&looseLep2Pt,"looseLep2Pt/F");
	treePtr->Branch("looseLep2Eta",&looseLep2Eta,"looseLep2Eta/F");
	treePtr->Branch("looseLep2Phi",&looseLep2Phi,"looseLep2Phi/F");
	treePtr->Branch("diLepMass",&diLepMass,"diLepMass/F");
	treePtr->Branch("nTau",&nTau,"nTau/I");
  for (auto beta : betas) {
    for (auto N : Ns) {
      for (auto order : orders) {
        TString ecfn(makeECFString(order,N,beta));
        treePtr->Branch("fj1"+ecfn,&(fj1ECFNs[ecfn]),"fj1"+ecfn+"/F");
      }
    }
  }
//ENDWRITE
}
